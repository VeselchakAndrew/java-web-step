package ua.com.danit.step.DAO;

import ua.com.danit.step.entities.UserLogin;
import ua.com.danit.step.utils.DBWorker;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;

import static ua.com.danit.step.entities.ExceptionHandler.SQLExceptionHandler;

public class CookiesDAO {
    DBWorker dbWorker = new DBWorker();

    public boolean cookiesValidate(UserLogin userLogin) throws ClassNotFoundException {

        boolean status = false;
        String SQLQuery = "select * from users where id = ? and login = ?";


        try {
            PreparedStatement preparedStatement = dbWorker.getConnection().prepareStatement(SQLQuery);
            preparedStatement.setString(1, userLogin.getId());
            preparedStatement.setString(2, userLogin.getUsername());
            ResultSet rs = preparedStatement.executeQuery();

            status = rs.next();
            System.out.println(rs);


        } catch (SQLException e) {
            SQLExceptionHandler(e);
        }

        return status;
    }

    public void cookiesCreate(String login, String id, HttpServletResponse response) {
        Cookie cookieName = new Cookie("name", login);
        Cookie cookieId = new Cookie("id", id);

        cookieName.setMaxAge(1209600);
        cookieId.setMaxAge(1209600);

        response.addCookie(cookieName);
        response.addCookie(cookieId);
    }

    public void deleteCookies(HttpServletResponse response) {
        Cookie cookieName = new Cookie("name", "");
        Cookie cookieId = new Cookie("id", "");

        cookieName.setMaxAge(0);
        cookieId.setMaxAge(0);

        response.addCookie(cookieName);
        response.addCookie(cookieId);
    }
}
