package ua.com.danit.step.DAO;

import ua.com.danit.step.entities.UserLogin;
import ua.com.danit.step.utils.DBWorker;

import java.sql.*;

import static ua.com.danit.step.entities.ExceptionHandler.SQLExceptionHandler;

public class LoginDAO {

    private static final DBWorker dbWorker = new DBWorker();

    public boolean validate(UserLogin userLogin) {

        String SQLQuery = "select * from users where login = ? and password = ?";
        boolean status = false;


        try {
            PreparedStatement preparedStatement = dbWorker.getConnection().prepareStatement(SQLQuery);
            preparedStatement.setString(1, userLogin.getUsername());
            preparedStatement.setString(2, userLogin.getPassword());
            ResultSet rs = preparedStatement.executeQuery();

            status = rs.next();
            System.out.println(rs);


        } catch (SQLException e) {
            SQLExceptionHandler(e);
        }

        return status;
    }
}
