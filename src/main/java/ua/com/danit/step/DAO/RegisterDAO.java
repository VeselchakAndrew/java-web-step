package ua.com.danit.step.DAO;

import ua.com.danit.step.entities.Profile;
import ua.com.danit.step.utils.Constants;
import ua.com.danit.step.utils.DBWorker;

import java.sql.*;

import static ua.com.danit.step.entities.ExceptionHandler.SQLExceptionHandler;

public class RegisterDAO {

    private static final DBWorker dbWorker = new DBWorker();

    public boolean isUserExist(String userLogin) throws ClassNotFoundException {
        String SQLQuery = "select * from users where login = ?";

        boolean status = false;


        try {
            PreparedStatement preparedStatement = dbWorker.getConnection().prepareStatement(SQLQuery);
            preparedStatement.setString(1, userLogin);
            ResultSet rs = preparedStatement.executeQuery();
            status = rs.next();
            System.out.println(status);

        } catch (SQLException e) {
            SQLExceptionHandler(e);
        }

        return status;
    }


    public void registerUser(Profile profile) throws ClassNotFoundException {
        String INSERT_USERS_SQL = "INSERT INTO users" +
                "  (id, name, last_name, avatar, login, password) VALUES " +
                " (?, ?, ?, ?, ?, ?);";


        try (Connection connection = dbWorker.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USERS_SQL)) {
            preparedStatement.setString(1, profile.getId());
            preparedStatement.setString(2, profile.getName());
            preparedStatement.setString(3, profile.getLastname());
            preparedStatement.setString(4, profile.getAvatar());
            preparedStatement.setString(5, profile.getUserName());
            preparedStatement.setString(6, profile.getUserPassword());

            System.out.println(preparedStatement);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            SQLExceptionHandler(e);
        }
    }

}
