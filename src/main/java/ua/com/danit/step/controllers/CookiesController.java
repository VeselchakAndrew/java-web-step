package ua.com.danit.step.controllers;

import ua.com.danit.step.DAO.CookiesDAO;
import ua.com.danit.step.entities.UserLogin;

import javax.servlet.http.HttpServletResponse;

public class CookiesController {
    private final CookiesDAO cookiesDAO = new CookiesDAO();

    public boolean cookiesValidate(UserLogin userLogin) throws ClassNotFoundException {
        return cookiesDAO.cookiesValidate(userLogin);
    }

    public void cookiesCreate(String login, String id, HttpServletResponse response) {
        cookiesDAO.cookiesCreate(login, id, response);
    }

    public void deleteCookies(HttpServletResponse response) {
        cookiesDAO.deleteCookies(response);
    }
}
