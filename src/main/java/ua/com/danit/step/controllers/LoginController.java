package ua.com.danit.step.controllers;

import ua.com.danit.step.DAO.LoginDAO;
import ua.com.danit.step.entities.UserLogin;

public class LoginController {
    LoginDAO loginDAO = new LoginDAO();

    public boolean validate(UserLogin userLogin) throws ClassNotFoundException {
        return loginDAO.validate(userLogin);
    }
}
