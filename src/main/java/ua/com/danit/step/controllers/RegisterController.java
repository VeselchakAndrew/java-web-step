package ua.com.danit.step.controllers;

import ua.com.danit.step.DAO.RegisterDAO;
import ua.com.danit.step.entities.Profile;

public class RegisterController {
    private final RegisterDAO registerDAO = new RegisterDAO();

    public boolean isUserExist(String userLogin) throws ClassNotFoundException {
        return registerDAO.isUserExist(userLogin);
    }

    public void registerUser(Profile profile) throws ClassNotFoundException {
        registerDAO.registerUser(profile);
    }
}

