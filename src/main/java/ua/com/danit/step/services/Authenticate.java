package ua.com.danit.step.services;

interface Authenticate {
    boolean isUserNamePresent(String name) throws ClassNotFoundException;

    boolean isAuthenticated(String name, String password) throws ClassNotFoundException;
}
