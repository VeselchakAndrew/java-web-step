package ua.com.danit.step.services;

import org.apache.commons.codec.digest.DigestUtils;

import java.util.HashMap;
import java.util.Map;

public class AuthenticateService implements Authenticate {
    private final Map<String, String> userProfileDara = new HashMap<>();


    {
        userProfileDara.put("admin", DigestUtils.md5Hex("12345"));
    }

    @Override
    public boolean isUserNamePresent(String name) throws ClassNotFoundException {
        Class.forName("org.postgresql.Driver");

        return userProfileDara.containsKey(name);
    }

    @Override
    public boolean isAuthenticated(String name, String password) throws ClassNotFoundException {
        if (!isUserNamePresent(name)) return false;

        return userProfileDara.get(name).equals(DigestUtils.md5Hex(password));
    }
}
