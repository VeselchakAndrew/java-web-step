package ua.com.danit.step.webserver;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.SessionIdManager;
import org.eclipse.jetty.server.session.DefaultSessionIdManager;
import org.eclipse.jetty.server.session.SessionHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import ua.com.danit.step.DAO.CollectionProfileDAO;
import ua.com.danit.step.controllers.ProfileController;
import ua.com.danit.step.services.ProfileService;
import ua.com.danit.step.utils.FreemarkerEngine;
import ua.com.danit.step.webserver.filters.LoginFilter;
import ua.com.danit.step.webserver.servlets.*;

import javax.servlet.DispatcherType;
import java.util.EnumSet;

public class Webserver {
  private final int port;
  private final String templatesDirectory;

  public Webserver(int port, String templatesDirectory) {
    this.port = port;
    this.templatesDirectory = templatesDirectory;
  }

  public void start() throws Exception {
    Server srv= new Server(port);

    FreemarkerEngine cfg = FreemarkerEngine.directory(templatesDirectory);
    ProfileController pc = new ProfileController(new ProfileService(new CollectionProfileDAO()));

    ServletContextHandler handler = new ServletContextHandler();

    SessionIdManager idmanager = new DefaultSessionIdManager(srv);
    srv.setSessionIdManager(idmanager);
    // Specify the session handler
    SessionHandler sessionsHandler = new SessionHandler();
    handler.setHandler(sessionsHandler);

    handler.addFilter(LoginFilter.class, "/*", EnumSet.of(DispatcherType.INCLUDE, DispatcherType.REQUEST));
    handler.addServlet(RootServlet.class, "/");

    handler.addServlet(new ServletHolder(new ProfilesServlet(cfg, pc)),"/users");
    handler.addServlet(new ServletHolder(new LikesListServlet(cfg, pc)),"/likes");
    handler.addServlet(new ServletHolder(new ChatServlet(cfg, pc)),"/messages/*");
    handler.addServlet(new ServletHolder(new LoginServlet(cfg)),"/login");
    handler.addServlet(new ServletHolder(new StartServlet(cfg)),"/start");
    handler.addServlet(new ServletHolder(new RegisterServlet(cfg)),"/register");
    handler.addServlet(new ServletHolder(new LogoutServlet(cfg)),"/logout");

    srv.setHandler(handler);
    srv.start();
    srv.join();
  }
}
