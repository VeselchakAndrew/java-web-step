package ua.com.danit.step.webserver.servlets;

import ua.com.danit.step.controllers.CookiesController;
import ua.com.danit.step.controllers.LoginController;
import ua.com.danit.step.entities.UserLogin;
import ua.com.danit.step.utils.DBWorker;
import ua.com.danit.step.utils.FreemarkerEngine;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;


public class LoginServlet extends HttpServlet {
    private final FreemarkerEngine fe;
    private final HashMap<String, Object> data = new HashMap<>();
    private final LoginController loginController = new LoginController();
    private final CookiesController cookiesController = new CookiesController();
    private final DBWorker dbWorker = new DBWorker();

    public LoginServlet(FreemarkerEngine fe) {
        this.fe = fe;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        fe.render("login.ftl", data, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String currentUserName = req.getParameter("login");
        String currentUserPassword = req.getParameter("password");
        UserLogin userLogin = new UserLogin();
        userLogin.setUsername(currentUserName);
        userLogin.setPassword(currentUserPassword);

        try {
            if (loginController.validate(userLogin)) {
                String SQLQuery = "select id from users where login = ?";
                PreparedStatement preparedStatement = dbWorker.getConnection().prepareStatement(SQLQuery);
                preparedStatement.setString(1, userLogin.getUsername());
                ResultSet rs = preparedStatement.executeQuery();

                while (rs.next()) {
                    userLogin.setId(rs.getString("id"));
                }

                System.out.println(userLogin);

                HttpSession session = req.getSession();
                session.setAttribute("username", currentUserName);

                cookiesController.cookiesCreate(userLogin.getUsername(), userLogin.getId(), resp);
                resp.sendRedirect("users");

            } else {
                resp.setCharacterEncoding("Windows-1251");
                resp.getWriter().write("Неверный логин или пароль");

            }

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

    }
}
