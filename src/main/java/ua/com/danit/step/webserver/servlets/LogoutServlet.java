package ua.com.danit.step.webserver.servlets;

import ua.com.danit.step.controllers.CookiesController;
import ua.com.danit.step.utils.FreemarkerEngine;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

public class LogoutServlet extends HttpServlet {
    private final FreemarkerEngine fe;
    private final CookiesController cookiesController = new CookiesController();
    private final HashMap<String, Object> data = new HashMap<>();

    public LogoutServlet(FreemarkerEngine fe) {
        this.fe = fe;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        req.getSession().invalidate();
        cookiesController.deleteCookies(resp);
        resp.sendRedirect("/start");
    }
}
