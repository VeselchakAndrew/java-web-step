package ua.com.danit.step.webserver.servlets;

import ua.com.danit.step.controllers.CookiesController;
import ua.com.danit.step.controllers.RegisterController;
import ua.com.danit.step.entities.Profile;
import ua.com.danit.step.utils.FreemarkerEngine;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;

public class RegisterServlet extends HttpServlet {
    private final FreemarkerEngine fe;
    private static Profile newUser;
    private static final CookiesController cookiesController = new CookiesController();

    private final RegisterController registerController = new RegisterController();

    public RegisterServlet(FreemarkerEngine fe) {
        this.fe = fe;
    }

    public static void renderProfile(Profile profile, FreemarkerEngine fe, HttpServletResponse resp) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("newUser", newUser);
        data.put("app_error", "");
        fe.render("userProfile.ftl", data, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        System.out.println(newUser);
        renderProfile(newUser, fe, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String defaultAvatar = "https://vk-wiki.ru/wp-content/uploads/2019/06/user-1.png";
        String userName = req.getParameter("name");
        String lastUserName = req.getParameter("lastname");
        String userLogin = req.getParameter("login");
        String userPassword = req.getParameter("password");
        newUser = new Profile(userName, lastUserName, defaultAvatar, userLogin, userPassword);


        try {
            if (!registerController.isUserExist(userLogin)) {
                try {
                    registerController.registerUser(newUser);
                    HttpSession session = req.getSession();
                    session.setAttribute("username", newUser.getUserName());
                    cookiesController.cookiesCreate(newUser.getUserName(), newUser.getId(), resp);
                    doGet(req, resp);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                resp.setCharacterEncoding("Windows-1251");
                resp.getWriter().write("Пользователь с таким именем уже зарегистрирован");
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


//

    }
}
