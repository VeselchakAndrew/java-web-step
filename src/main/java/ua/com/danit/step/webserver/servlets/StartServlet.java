package ua.com.danit.step.webserver.servlets;

import ua.com.danit.step.controllers.CookiesController;
import ua.com.danit.step.entities.UserLogin;
import ua.com.danit.step.utils.FreemarkerEngine;

import javax.servlet.http.*;
import java.io.IOException;
import java.util.Objects;

public class StartServlet extends HttpServlet {

    private final CookiesController cookiesController = new CookiesController();
    private final UserLogin userLogin = new UserLogin();


    public StartServlet(FreemarkerEngine fe) {
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        Cookie[] cookies = req.getCookies();
        String userName = "";
        String userId = "";

        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (Objects.equals(cookie.getName(), "name")) {
                    userName = cookie.getValue();
                } else if (Objects.equals(cookie.getName(), "id")) {
                    userId = cookie.getValue();
                }
            }
        } else {
            resp.sendRedirect("/login");
        }


        userLogin.setUsername(userName);
        userLogin.setId(userId);
        System.out.println("User Id " + userId);

        try {
            if (cookiesController.cookiesValidate(userLogin)) {
                HttpSession session = req.getSession();
                session.setAttribute("username", userLogin.getUsername());
                resp.sendRedirect("/users");
            } else {
                resp.sendRedirect("/login");
            }

        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }

    }
}
