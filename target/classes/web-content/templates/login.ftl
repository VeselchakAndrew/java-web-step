<#import "common.ftl" as c>
<@c.page>
    <div class="wrapper">

        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="login-tab" data-toggle="tab" href="#login" aria-controls="login" aria-selected="true">Login</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="register-tab" data-toggle="tab" href="#register" aria-controls="register" aria-selected="false">Register</a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="login" role="tabpanel" aria-labelledby="login-tab">
                <div class="login">
                    <h3>Login</h3>
                    <form action="login" method="post">
                        <p><input type="text" name="login" placeholder="login"></p>
                        <p><input type="password" name="password" placeholder="password"></p>
                        <p>Remember me <input type="checkbox" name="remember"></p>
                        <p><input type="submit" value="Login"></p>
                    </form>
                </div>
            </div>

            <div class="tab-pane" id="register" role="tabpanel" aria-labelledby="register-tab">
                <div class="login">
                    <h3>Register</h3>
                    <div>
                        <form action="register" method="post">
                            <p><input type="text" name="name" placeholder="Name"></p>
                            <p><input type="text" name="lastname" placeholder="Last Name"></p>
                            <p><input type="text" name="login" placeholder="login"></p>
                            <p><input type="password" name="password" placeholder="password"></p>
                            <p><input type="submit" value="Register"></p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@c.page>