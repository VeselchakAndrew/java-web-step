<#import "common.ftl" as c>
<@c.page>
    <div class="user_profile">
        <h1 class="userprofile_text_info">UserProfile</h1>
        <div class="userprofile">


            <div class="userprofile_avatar">
                <img src="${newUser.avatar}" alt="User photo" class="userprofile_avatar_img">
            </div>
            <div class="user_info">
                <p class="userprofile_text_info">Имя: ${newUser.name}</p>
                <p class="userprofile_text_info">Фамилия: ${newUser.lastname}</p>
            </div>

            <div>
                <p class="userprofile_text_info">Перейти к выбору понравившихся профилей</p>
                <div class="userprofile_btn_block">
                    <a class="user_btn" href="/users">Start!</a>
                </div>
            </div>
        </div>
    </div>
</@c.page>